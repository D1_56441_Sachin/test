const express = require("express");

const app = express();

const cors = require("cors");
const routerStud = require("./route/Stud");
app.use(cors("*"));
app.use(express.json());
app.use("/Stud", routerStud);

app.listen(4000, "0.0.0.0", () => {
  console.log("server started on port 4000");
});
