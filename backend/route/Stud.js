const { query } = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");

// display all data of employee
router.get("/:name", (request, response) => {
  const { name } = request.params;

  const connection = utils.openConnection();

  const statement = `
        select * from Stud where
        name = '${name}'
      `;
  connection.query(statement, (error, result) => {
    //connection.end();
    if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("user not found !");
    }
  });
});

router.post("/add", (request, response) => {
  const { name, Class, age } = request.body;

  const connection = utils.openConnection();
  console.log(connection);
  const statement = `
        insert into Stud
          ( name,Class,age)
        values
          ( '${name}',${Class},${age})
      `;
  connection.query(statement, (error, result) => {
    //connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:name", (request, response) => {
  const { name } = request.params;
  const { Class } = request.body;

  const statement = `
    update Stud
    set
      salary=${Class}
    where
      name = '${name}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
   // connection.end();
    console.log(statement);

    response.send(utils.createResult(error, result));
  });
});
router.delete("/remove/:name", (request, response) => {
  const { name } = request.params;
  const statement = `
    delete from Stud
    where
      name = '${name}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    // connection.end();
    console.log(statement);
    response.send(utils.createResult(error, result));
  });
});
module.exports = router;
